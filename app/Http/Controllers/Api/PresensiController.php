<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Presensi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data = Presensi::join('kelas','kelas.id','=','presensi.kelas_id')
                    ->join('siswa','siswa.id','=','presensi.siswa_id')
                    ->select([
                        'presensi.id',
                        'kelas.nama_kelas',
                        'siswa.nis',
                        'siswa.nama',
                        'presensi.absensi',
                        'presensi.keterangan',
                    ])->get();

        return $this->success($data,200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kelas_id' => 'required|exists:kelas,id',
            'siswa_id' => 'required|exists:siswa,id',
             'absensi' => 'required|in:hadir,izin,absen,telat',
            'keterangan' => 'nullable|string'
        ]);

        if ($validator->fails()) {
            $msg =$validator->errors();

            return $this->failedResponse($msg,422);

        }
        $presensi = new Presensi();
        $presensi->kelas_id = $request->kelas_id;
        $presensi->siswa_id = $request->siswa_id;
        $presensi->absensi = $request->absensi;
        $presensi->keterangan = $request->keterangan;
        $tambahPresensi = $presensi->save();
        if ($tambahPresensi) {
            return $this->success($presensi,201);
        }else{
            return $this->failedResponse('User gagal ditambahkan!',500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function show(Presensi $presensi)
    {
        return $this->success($presensi,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presensi $presensi)
    {
       $validator = Validator::make($request->all(), [
            'kelas_id' => 'required|exists:kelas,id',
            'siswa_id' => 'required|exists:siswa,id',
             'absensi' => 'required|in:hadir,izin,absen,telat',
            'keterangan' => 'nullable|string'
        ]);

        if ($validator->fails()) {
            $msg =$validator->errors();

            return $this->failedResponse($msg,422);

        }

        $presensi->kelas_id = $request->kelas_id;
        $presensi->siswa_id = $request->siswa_id;
        $presensi->absensi = $request->absensi;
        $presensi->keterangan = $request->keterangan;
        $editPresensi = $presensi->save();
        if ($editPresensi) {
            return $this->success($presensi,201);
        }else{
            return $this->failedResponse('Presensi gagal ditambahkan!',500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presensi $presensi)
    {
        $deleteData = $presensi->delete();

        if ($deleteData) {
            return $this->success(null,200);
        }else{
            return $this->failedResponse('User gagal dihapus!',500);
        }
    }
    private function
    success($data,$statusCode,$message='success')
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ],$statusCode);
 }
    private function
    failedResponse($message,$statusCode)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
    ],$statusCode);
    }
    /*public function main()
    {
          $data = Presensi::join('kelas','kelas.id','=','presensi.kelas_id')
                    ->join('siswa','siswa.id','=','presensi.siswa_id')
                    ->select([
                        'presensi.id',
                        'kelas.nama_kelas',
                        'siswa.nis',
                        'siswa.nama',
                        'presensi.absensi',
                        'presensi.keterangan',
                    ])->get();

        return $this->success($data,200);
    }*/
}
