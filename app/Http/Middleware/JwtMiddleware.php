<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
        }
        catch (Exppection $e) {
            if ($e instanceof \Tymon|JwtAuth\Exceptions\TokenInvalidException){
                return $this->failedResponse('Token anda tidak Valid',401);
            }else if ($e instanceof \Tymon|JwtAuth\Exceptions\TokenInvalidException){
                return $this->failedResponse('Token is Expired',401);
            }else {
                return $this->failedResponse('Authorization Token not found',401);
                
            }

        }
        return $next($request);
    }
    private function
    failedResponse($message,$statusCode)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
    ],$statusCode);
    }
}