<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
 protected $table = 'presensi';
 protected $fillable = [ 'kelas_id','siswa_id','absensi','keterangan' ];
 public function kelas()
 {
 return $this->belongsTo('App\Model\Kelas','kelas_id');
 }

 public function siswas()
 {
 return $this->belongsTo('App\Model\Siswa','siswa_id');
 }
}
